# jmare-maven-repo

Please use following Maven configuration in your projects to access the jmare libraries:

```
    <repositories>
        <repository>
            <id>jmare_repository</id>
            <url>https://bitbucket.org/janmaren/jmare-maven-repo/raw/master/repository/</url>
        </repository>
    </repositories>
```
