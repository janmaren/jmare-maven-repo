<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>cz.jmare.graphfw</artifactId>
    <parent>
        <groupId>cz.jmare</groupId>
        <artifactId>cz.jmare</artifactId>
        <version>1.17</version>
    </parent>
	
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
				    <source>1.8</source>
				    <target>1.8</target>
				</configuration>
			</plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>			
		</plugins>
	</build>
	<dependencies>
        <dependency>
             <groupId>org.eclipse.jface</groupId>
             <artifactId>org.eclipse.jface</artifactId>
             <version>3.18.0.v20191122-2109</version>
        </dependency>
        <dependency>
             <groupId>org.eclipse.osgi</groupId>
             <artifactId>org.eclipse.osgi</artifactId>
             <version>3.15.100.v20191114-1701</version>
        </dependency>
        <dependency>
             <groupId>org.eclipse.core.commands</groupId>
             <artifactId>org.eclipse.core.commands</artifactId>
             <version>3.9.600.v20191122-2109</version>
        </dependency>
        <dependency>
             <groupId>org.eclipse.equinox.common</groupId>
             <artifactId>org.eclipse.equinox.common</artifactId>
             <version>3.10.600.v20191004-1420</version>
        </dependency>

		<dependency>
		    <groupId>com.thoughtworks.xstream</groupId>
		    <artifactId>xstream</artifactId>
		    <version>1.4.10</version>
		</dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.3</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.25</version>
        </dependency>

        <dependency>
            <groupId>cz.jmare</groupId>
            <artifactId>cz.jmare.core</artifactId>
            <version>1.17</version>
        </dependency>

        <dependency>
            <groupId>cz.jmare</groupId>
            <artifactId>cz.jmare.swt</artifactId>
            <version>1.17</version>
        </dependency>

        <dependency>
            <groupId>cz.jmare</groupId>
            <artifactId>cz.jmare.ai</artifactId>
            <version>1.17</version>
        </dependency>
        
        <dependency>
            <groupId>cz.jmare</groupId>
            <artifactId>cz.jmare.math</artifactId>
            <version>1.17</version>
        </dependency>
	</dependencies>
	<profiles>
		<profile>
			<id>linux-x86-64</id>
            <activation>
                <os>
                    <name>linux</name>
                    <arch>amd64</arch>
                </os>
            </activation>
			<dependencies>
                <dependency>
                     <groupId>org.eclipse.swt.gtk.linux.x86_64</groupId>
                     <artifactId>org.eclipse.swt.gtk.linux.x86_64</artifactId>
                     <version>3.113.0.v20191204-0601</version>
                </dependency>
			</dependencies>
			<properties>
	            <jar-suffix>linux-x86-64</jar-suffix>
	        </properties>
		</profile>
		<profile>
			<id>win-x86-64</id>
            <activation>
                <os>
                    <family>Windows</family>
                    <arch>amd64</arch>
                </os>
            </activation>
			<dependencies>
                <dependency>
                     <groupId>org.eclipse.swt.win32.win32.x86_64</groupId>
                     <artifactId>org.eclipse.swt.win32.win32.x86_64</artifactId>
                     <version>3.113.0.v20191204-0601</version>
                </dependency>
			</dependencies>
			<properties>
	            <jar-suffix>win-x86-64</jar-suffix>
	        </properties>
		</profile>
        <profile>
            <id>macosx-x86-64</id>
            <activation>
                <os>
                    <family>mac</family>
                    <arch>amd64</arch>
                </os>
            </activation>
            <dependencies>
                <dependency>
                    <groupId>org.eclipse.swt.cocoa.macosx.x86_64</groupId>
                    <artifactId>org.eclipse.swt.cocoa.macosx.x86_64</artifactId>
                    <version>3.105.0</version>
                </dependency>
            </dependencies>
            <properties>
                <jar-suffix>macosx-x86-64</jar-suffix>
            </properties>
        </profile>
	</profiles>

    <repositories>
        <repository>
            <id>standovo_repo</id>
            <url>https://bitbucket.org/stmare/jmare-maven-repo/raw/master/repository/</url>
        </repository>
    </repositories>
</project>
